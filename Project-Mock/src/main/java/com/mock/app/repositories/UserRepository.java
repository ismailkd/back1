package com.mock.app.repositories;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mock.app.entities.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

	UserEntity findById(long id);
	
	UserEntity findByEmail(String email);
	
	List<UserEntity> findByFirstName(String firstName);
	
	UserEntity findByUserId(String userId);
	
	//Page<UserEntity> findAllUsers(Pageable pageableRequest);
	
}
