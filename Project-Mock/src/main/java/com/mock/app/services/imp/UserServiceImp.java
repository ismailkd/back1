package com.mock.app.services.imp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


import com.mock.app.Shared.Utils;
import com.mock.app.entities.UserEntity;
import com.mock.app.repositories.UserRepository;
import com.mock.app.services.UserService;

@Service
public class UserServiceImp implements UserService {

	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	Utils util;
	
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		UserEntity userEntity = userRepository.findByEmail(email);
		if(userEntity == null) throw new UsernameNotFoundException(email);
		
		return new User(userEntity.getEmail(), userEntity.getEncryptePassword(), new ArrayList<>());
		
	}



	@Override
	public UserEntity getUser(String email) {
		
		UserEntity userEntity = userRepository.findByEmail(email);
		if(userEntity == null) throw new UsernameNotFoundException(email);
		
		return userEntity;
		
	}



	@Override
	public UserEntity createUser(UserEntity user) {
		
		UserEntity checkUser = userRepository.findByEmail(user.getEmail());
		if(checkUser != null) throw new RuntimeException("user already exist !"); 
		
		UserEntity userEntity = new UserEntity();
		
		userEntity.setEncryptePassword(bCryptPasswordEncoder.encode(user.getPassword())); // crypter le passwd
		userEntity.setUserId(util.generateStringId(32)); // on veut generer une chaine de 32 caractere 
		userEntity.setEmail(user.getEmail());
		userEntity.setFirstName(user.getFirstName());
		userEntity.setLastName(user.getLastName());
		userEntity.setPassword(user.getPassword());
		
		UserEntity newUser = userRepository.save(userEntity); // la persistance dans la bd 
		
		return newUser;
		
	}
	



	@Override
	public UserEntity getUserByUserId(String userId) {
		
		UserEntity userEntity = userRepository.findByUserId(userId);
		if(userEntity == null) throw new UsernameNotFoundException(userId);
		
		return userEntity;
	}



	@Override
	public UserEntity updateUser(String userId, UserEntity userEntity) {
		
		UserEntity user = userRepository.findByUserId(userId);
		if(user == null) throw new UsernameNotFoundException(userId);
		
		user.setFirstName(userEntity.getFirstName());
		user.setLastName(userEntity.getLastName());
		user.setEmail(userEntity.getEmail());
	
		
		UserEntity userUpdated = userRepository.save(user);
		
		return userUpdated;
	}
	



	@Override
	public void deleteUser(String userId) {
		
		UserEntity userEntity = userRepository.findByUserId(userId);
		if(userEntity == null) throw new UsernameNotFoundException(userId);
		
		userRepository.delete(userEntity);
		
	}



	@Override
	public List<UserEntity> getUsers() {
		
		List<UserEntity> users = userRepository.findAll();
		return users;
		
	}
	
	

}
