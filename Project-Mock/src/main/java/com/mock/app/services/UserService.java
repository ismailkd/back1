package com.mock.app.services;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;


import com.mock.app.entities.UserEntity;

public interface UserService  extends UserDetailsService{
	
	UserEntity getUser(String email);
	
	UserEntity createUser(UserEntity userDto);

	UserEntity getUserByUserId(String userId);
	
	UserEntity updateUser(String userId, UserEntity userEntity);
	
	void deleteUser(String userId);                     
	
	List<UserEntity> getUsers();

}
