package com.mock.app.controllers;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mock.app.Shared.Utils;
import com.mock.app.entities.UserEntity;

import com.mock.app.services.UserService;


@RequestMapping("/users")
@RestController
public class UserController {
	
	

	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	UserService userService;
	
	@Autowired
	Utils util;
	
	@GetMapping
	public List<UserEntity> getUsers() {

		return userService.getUsers();
		
	}
	
	@GetMapping(path= "/{id}")
	public UserEntity getUser(@PathVariable String id){
		
		return userService.getUserByUserId(id);
	}
	
	@PostMapping
	public UserEntity saveUser(@RequestBody UserEntity user){
		
		return userService.createUser(user);
	}
	
	
	@DeleteMapping(path= "/{id}")
	public String deleteUser(@PathVariable String id) {
		
		userService.deleteUser(id);
	
		return "user was deleted !!";
	}
	
	
	
	@PutMapping(path= "/{id}")
	public UserEntity updateUser(@PathVariable String id, @RequestBody UserEntity newUser) {
		
		return userService.updateUser(id, newUser);
		
	}
	
	
	
	
	
	

}
