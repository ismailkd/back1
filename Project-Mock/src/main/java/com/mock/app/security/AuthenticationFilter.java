package com.mock.app.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.mock.app.SpringApplicationContext;
import com.mock.app.entities.UserEntity;
import com.mock.app.requests.UserLoginRequest;
import com.mock.app.services.UserService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private final AuthenticationManager authenticationManager;
	
	@Autowired
	UserService userService;
	
	public AuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) // il se declenche  apres l'execution du users/Login avec post    																																																
			throws AuthenticationException {
		try {

			UserLoginRequest creds = new ObjectMapper().readValue(req.getInputStream(), UserLoginRequest.class);

			return authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(creds.getEmail(), creds.getPassword(), new ArrayList<>()));

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
			Authentication auth) throws IOException, ServletException {

		String userName = ((User) auth.getPrincipal()).getUsername();
		UserService userService = (UserService)SpringApplicationContext.getBean("userServiceImp");   

		UserEntity userEntity = userService.getUser(userName);

		String token = Jwts.builder().setSubject(userName)
				.claim("id",userEntity.getUserId())
				.claim("name", userEntity.getFirstName() + " " + userEntity.getLastName())
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SecurityConstants.TOKEN_SECRET).compact();

		

 		res.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token);
		res.addHeader("user_id", userEntity.getUserId());
		res.addHeader("name", userEntity.getFirstName());

		res.getWriter().write("{ \"name\": \"" + userEntity.getFirstName() + "\", \"token\": \"" + token + "\", \"id\": \""+ userEntity.getUserId() + "\"}"); 
		
		//res.getWriter().write("{ \"token\": \"" + token + "\", \"id\": \""+ userEntity.getUserId() + "\"}"); 
	}


	
}
