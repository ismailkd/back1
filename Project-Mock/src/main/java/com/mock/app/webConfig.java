package com.mock.app;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class webConfig implements WebMvcConfigurer {

	public void addCorsMappings(CorsRegistry registry) {
		
		
		
		// pour personaliser 
			/*
			registry
			.addMapping("/users")
			.allowedMethods("Get","PUT","POST")
			.allowedOrigins("http://localhost:4200");
			 */
			
			 
		//pour donner l'acces a tous les routes, methodes, et domaines	
			
			registry
			.addMapping("/**")
			.allowedMethods("*")
			.allowedOrigins("*");
		
			
		}

	
}
